  #include <PID_v1.h> // by brett beauregard

const int sensorPin = A0;   // Pino ao qual o sensor de luminosidade está conectado
const int ledPin = 9;       // Pino ao qual o LED (ou saída de luminosidade) está conectado

const int potPinKp = A1;     // Pino ao qual o potenciômetro para Kp está conectado
const int potPinKi = A2;     // Pino ao qual o potenciômetro para Ki está conectado
const int potPinKd = A3;     // Pino ao qual o potenciômetro para Kd está conectado
const int potPinSp = A4;

double Sp;
double input, output;
double Kp, Ki, Kd;
PID pid(&input, &output, &Sp, Kp, Ki, Kd, DIRECT);


void setup() {
  Serial.begin(9600);
  pinMode(sensorPin, INPUT);
  pinMode(ledPin, OUTPUT);
  
  
  // Inicializa os potenciômetros
  Kp = map(analogRead(potPinKp), 0, 1023, 0, 10);  // Mapeia para um valor entre 0 e 10
  Ki = map(analogRead(potPinKi), 0, 1023, 0, 10);
  Kd = map(analogRead(potPinKd), 0, 1023, 0, 10);
  Sp = map(analogRead(potPinSp), 0, 1023, 0, 100);

  pid.SetMode(AUTOMATIC);
}

void loop() {
  // Atualiza os valores dos potenciômetros a cada ciclo
  Kp = map(analogRead(potPinKp), 0, 1023, 0, 10);
  Ki = map(analogRead(potPinKi), 0, 1023, 0, 10);
  Kd = map(analogRead(potPinKd), 0, 1023, 0, 10);
  Sp = map(analogRead(potPinSp), 0, 1023, 0, 100);
  //input = analogRead(sensorPin);
 float valor_analog_lm35 = float(analogRead(sensorPin)); // Obtém o valor analógico que varia de 0 a 1023
float tensao = (valor_analog_lm35 * 5) / 1023; // Vamos converter esse valor para tensão elétrica
float temperatura = tensao / 0.010; // dividimos a tensão por 0.010 que representa os 10 mV
 input = temperatura;
  pid.SetTunings(Kp, Ki, Kd);
  pid.Compute();
  analogWrite(ledPin, output);
  Serial.println(input);
  Serial.print("Kp: ");
  Serial.print(Kp);
  Serial.print("\tKi: ");
  Serial.print(Ki);
  Serial.print("\tKd: ");
  Serial.print(Kd);
  Serial.print("\tlm35: ");
  Serial.println(temperatura);

 Serial.print("\tSp: ");
  Serial.println(Sp);

  delay (1000);
}
