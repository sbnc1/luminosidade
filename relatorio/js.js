const socket = io();

document.getElementById('luminosidade').addEventListener('input', function () {
  const luminosidadeAtual = this.value;
  socket.emit('luminosidade', luminosidadeAtual);
});

socket.on('atualizarLuminosidade', function (luminosidade) {
  const luminosidadeInput = document.getElementById('luminosidade');
  const luminosidadeValor = document.getElementById('luminosidadeValor');
  luminosidadeInput.value = luminosidade;
  luminosidadeValor.textContent = luminosidade.toFixed(2);
});
